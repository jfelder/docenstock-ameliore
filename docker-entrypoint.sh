#!/bin/sh
set -e

exec "$@" &

if [ ! -f /scraper/.initial_scrape ]; then
    echo "Initial Scraping"
    crontab /scraper/crontask
    python3 /scraper/scraper.py && touch /scraper/.initial_scrape  && cp /scraper/index.html /scraper/main.js /usr/share/nginx/html/
    echo "Ready !"

fi

cron -f
