#!/usr/bin/env python3
#
# Copyright 2019 Jean Felder
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with GNOME Music; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import argparse
from datetime import datetime as dt
import logging
import os

from bs4 import BeautifulSoup
import datefinder
import jinja2
import requests
from tqdm import tqdm

from utils import get_infos, clean_url

BASE_URL = 'https://www.sgdf.fr'
DOC_URL = BASE_URL + '/vos-ressources/doc-en-stock'


TEMPLATE_FILE = 'template.html'
DATE_FORMAT = '%d/%m/%Y'


def get_soup(session, url):
    """Get an url and parses the content with bs4.

    :param Session session: requests session
    :param str url: url
    :returns: parsed content
    :rtype: BeautifulSoup
    """
    r = session.get(url)
    return BeautifulSoup(r.text, 'html.parser')


def browse_subcategory(session, url, doc_dates, doc_nodates, category_index):
    """Recursively retrieve all the content of a subcategory.

    :param Session session: requests session
    :param str url: url
    :param dict doc_dates: Dcouments with a date
    :param dict doc_nodates: Documents without a date
    :param int category_index: category index
    :returns: None
    :rtype:
    """
    soup_sub = get_soup(session, url)
    sub_urls = []
    # test if subcategory has subcategories
    other_subcategories = soup_sub.find_all('div', {'class': 'pd-subcategory'})
    for other_subcategory in other_subcategories:
        link = other_subcategory.find('a')
        ref = clean_url(link['href'])
        sub_urls.append(BASE_URL + ref)

    # get files of current subcategory
    files = soup_sub.find_all('div', {'class': 'pd-filebox'})
    for file in files:
        download = file.find('div', {'class': 'pd-float'})
        link = download.find('a')
        ref = clean_url(link['href'])
        title = link.text.replace("\"", "")
        logging.debug("    - " + ref)
        creation_date = None
        # if ref not in do_not_scrape:
        display_date = file.find('div', {'class': 'pd-date'})
        if display_date is None:
            display_date = file.find('div', {'class': 'pd-fdesc'})
        if display_date is not None:
            matches = datefinder.find_dates(display_date.text)
            display_date = next(matches, None)

        creation_date, extension = get_infos(ref, url, display_date)
        if creation_date is not None:
            creation_date = creation_date.strftime(DATE_FORMAT)
            doc_dates.append({'titre': title,
                              'href': ref,
                              'cindex': str(category_index),
                              'date': creation_date,
                              'ext': extension})
        else:
            doc_nodates.append({'titre': title,
                                'href': ref,
                                'cindex': str(category_index)})

    # recursive - browse other subcategories:
    for sub_url in sub_urls:
        logging.debug("      - " + sub_url)
        browse_subcategory(
            session, sub_url, doc_dates, doc_nodates, category_index)


def main():
    # logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--debug', action='store_true', default=False, dest='debug')
    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    session = requests.Session()
    adapter = requests.adapters.HTTPAdapter(
        pool_connections=100, pool_maxsize=100)
    session.mount('https://', adapter)

    soup = get_soup(session, DOC_URL)
    doc_dates = []
    doc_nodates = []
    saved_categories = []

    # from main page get all categories
    logging.info("browsing all categories")
    categories = soup.find_all(
        'div', {'class': 'pd-categoriesbox large-4 columns'})
    cindex = 0
    for category in tqdm(categories, desc="categorie : "):
        category_title = category.find('div', {'class': 'pd-title'})
        category_title_text = category_title.text.strip()
        logging.debug("- catégorie : " + category_title_text)
        saved_categories.append(
            {'label': category_title_text, 'id': cindex})
        subcategories = category.find_all('div', {'class': 'pd-subcategory'})
        for subcategory in tqdm(subcategories, desc="sous-catégorie : "):
            logging.debug("  - " + subcategory.text.strip())
            # get links
            links = subcategory.find_all('a')
            for link in links:
                url = BASE_URL + link['href']
                # logging.debug("  - " + url)
                browse_subcategory(
                    session, url, doc_dates, doc_nodates, cindex)
        cindex += 1

    # generate html page
    logging.info("generating output html file")
    doc_dates.sort(
        key=lambda x: dt.strptime(x['date'], DATE_FORMAT), reverse=True)
    current_dir = os.path.dirname(os.path.realpath(__file__))
    template_loader = jinja2.FileSystemLoader(searchpath=current_dir)
    template_env = jinja2.Environment(loader=template_loader)

    template = template_env.get_template(TEMPLATE_FILE)
    date = dt.now().strftime("%d/%m/%Y %Hh%M")
    output_text = template.render(
        with_dates=doc_dates, without_dates=doc_nodates,
        categories=saved_categories, date=date)
    with open(os.path.join(current_dir, "index.html"), "wb") as fh:
        fh.write(output_text.encode('utf-8'))


if __name__ == '__main__':
    main()
